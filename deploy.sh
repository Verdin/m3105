#!/usr/bin/env bash

## configuration de wiki.org 

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans Wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp Wiki.org/named.conf dwikiorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans Wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp Wiki.org/* dwikiorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf 



## configuration de iut.re 

# La commande ci-dessous crée un répertoire /etc/named sur la machine diutre
himage diutre mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans iut.re/named.conf sur la machine diutre dans le répertoire /etc/
hcp iut.re/named.conf diutre:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans iut.re/* dans le répertoire  /etc/named sur la machine diutre
hcp iut.re/* diutre:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine diutre
himage diutre rm /etc/named/named.conf 



## configuration du TLD du serveur .org

# La commande ci-dessous crée un répertoire /etc/named sur la machine dorg
himage dorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans tldo/named.conf sur la machine dorg dans le répertoire /etc/
hcp tldorg/named.conf dorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans tldo/* dans le répertoire  /etc/named sur la machine dorg
hcp tldorg/* dorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dorg
himage dorg rm /etc/named/named.conf 



## configuration du TLD du serveur .re

# La commande ci-dessous crée un répertoire /etc/named sur la machine dre
himage dre mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans tldr/named.conf sur la machine dre dans le répertoire /etc/
hcp tldre/named.conf dre:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans tldr/* dans le répertoire  /etc/named sur la machine dre
hcp tldre/* dre:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dre
himage dre rm /etc/named/named.conf 



## configuration du serveur aRootServer

# La commande ci-dessous crée un répertoire /etc/named sur la machine aRootServeer
himage aRootServer mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans aRootServeer/named.conf sur la machine aRootServeer dans le répertoire /etc/
hcp aRootServer/named.conf aRootServer:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans aRootServeer/* dans le répertoire  /etc/named sur la machine aRootServeer
hcp aRootServer/* aRootServer:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine aRootServeer
himage aRootServer rm /etc/named/named.conf 
